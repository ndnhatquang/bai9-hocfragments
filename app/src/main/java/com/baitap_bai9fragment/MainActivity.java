package com.baitap_bai9fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.baitap_bai9fragment.fragments.FragmentA;
import com.baitap_bai9fragment.fragments.FragmentB;

import static com.baitap_bai9fragment.R.id.buttonA;
import static com.baitap_bai9fragment.R.id.buttonB;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnA= (Button) findViewById(buttonA);
        Button btnB= (Button) findViewById(buttonB);

        btnA.setOnClickListener(this);
        btnB.setOnClickListener(this);
    }

    private void addFragments(String key,boolean addtostack) {
        Fragment fragment=null;
        if(key.equalsIgnoreCase("A")){
            fragment=new FragmentA();
        }
        else {
            fragment=new FragmentB();
        }
        FragmentManager manager= getFragmentManager();
        FragmentTransaction ts=manager.beginTransaction();
        ts.replace(R.id.containt,fragment);
        if(addtostack)
        {
            ts.addToBackStack(key);
        }
        ts.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonA:
                addFragments("A",true);
                break;
            case R.id.buttonB:
                addFragments("B",true);
                break;

        }

    }
}
