package com.baitap_bai9fragment.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baitap_bai9fragment.R;

/**
 * Created by Quang Do on 10/11/2015.
 */
public class FragmentFile extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_file,null,false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
